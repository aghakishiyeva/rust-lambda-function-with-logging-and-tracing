use lambda_runtime::{handler_fn, Context, Error};
use serde::{Deserialize, Serialize};
use tracing::{debug, error, info, Level};
use tracing_subscriber::FmtSubscriber;

#[derive(Debug, Deserialize, Serialize)]
struct CustomEvent {
    message: String,
}

#[derive(Debug, Deserialize, Serialize)]
struct CustomResponse {
    message: String,
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    // Initialize the logging system with a subscriber that captures all log levels up to DEBUG.
    let subscriber = FmtSubscriber::builder()
        .with_max_level(Level::DEBUG) // Adjust to capture detailed logs
        .finish();

    tracing::subscriber::set_global_default(subscriber)
        .expect("Setting default subscriber failed");

    let func = handler_fn(func);
    lambda_runtime::run(func).await?;
    Ok(())
}

async fn func(event: CustomEvent, _context: Context) -> Result<CustomResponse, Error> {
    debug!("Debugging event: {:?}", event); // Detailed debugging information
    info!("Received event: {:?}", event); // General information

    // Simulate processing
    let response = CustomResponse {
        message: format!("Hello, {}", event.message),
    };

    if response.message.is_empty() {
        error!("Response message is empty."); // Error logging
    } else {
        info!("Responding with: {:?}", response); // Successful operation information
    }

    Ok(response)
}
