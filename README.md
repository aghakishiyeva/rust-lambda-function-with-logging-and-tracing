# Rust Lambda Function with Logging and Tracing

This repository contains a Rust Lambda function instrumented with logging and tracing capabilities, designed to run in the AWS Lambda environment.

## Project Overview

The Lambda function handles a custom event by generating a response message. It's instrumented with logging for observability using the `tracing` and `tracing-subscriber` crates. The func function serves as the entry point for event processing, logging detailed debugging information, event reception, errors, and successful operation details. Overall, it showcases a simple yet effective approach to implementing a Rust Lambda function with logging and error handling capabilities.

## Project Structure
- **src/main.rs**: Contains the main code for the Lambda function.
- **Cargo.toml**: Specifies project metadata and dependencies.
- **.cargo/config.toml**: Configures the build environment for cross-compiling to AWS Lambda's target architecture.

## Getting Started
1. Clone this repository to your local machine:

   ```bash
   git clone https://gitlab.com/aghakishiyeva/rust-lambda-function-with-logging-and-tracing.git
   ```

2. Ensure you have Rust installed:

   ```bash
   rustc --version
   ```

3. Build and run the project locally using Cargo:

   ```bash
   cargo run
   ```

## Deploying to AWS Lambda

1. Package and upload the Lambda function to AWS Lambda.
2. Create a new Lambda function in the AWS Management Console.
3. Ensure your Lambda function's execution role has the necessary permissions **to write logs to CloudWatch Logs**. You can configure this using the AWS CLI or AWS Management Console. Here's an example command to update the Lambda function's IAM role to grant permission for logging to CloudWatch Logs:

   ```bash
   aws lambda update-function-configuration \
   --function-name my-function \
   --region us-east-1 \
   --cli-input-json '{
      "Role": "arn:aws:iam::123456789012:role/lambda-role",
      "Environment": {
         "Variables": {
               "AWS_LAMBDA_EXECUTION_ROLE": "arn:aws:iam::123456789012:role/lambda-role",
               "AWS_XRAY_DAEMON_ADDRESS": "127.0.0.1:2000"
         }
      }
   }
   ```




